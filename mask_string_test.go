package main

import "testing"

func TestMask(t *testing.T) {
	//Act
	TestAdresses := []struct {
		in   string
		want string
	}{
		{"http://www.youtube.com", "http://***************"},
		{"http://www.google.com", "http://**************"},
		{"http://web.telegram.org", "http://****************"},
		{"http://habr.com/ru/companies/avito/articles/658907/", "http://********************************************"},
		{"http://lordserial.fun", "http://**************"},
		{"https://m.moreigr.com/8727-the-walking-dead-season-1.html", "https:/**************************************************"},
	}
	//Arrange & Assert
	for _, ta := range TestAdresses {
		inbyte := []byte(ta.in)
		result := Mask(inbyte)
		if result != ta.want {
			t.Errorf("Incorrect result. Expect %s, got %s", ta.want, result)
		}
	}
}

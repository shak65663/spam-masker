package main

import (
	"fmt"
)

func main() {
	// Ввод сообщения
	var adress string
	fmt.Printf("\rHere's my spammy page: ")
	fmt.Scanf("%s", &adress)

	//Представление ссылки в виде байтового среза

	byteadress := []byte(adress)

	maskstring := Mask(byteadress)

	fmt.Printf("Here's my spammy page: %s see you.", maskstring)
}

func Mask(a []byte) string {

	byteslice := []byte("")

	//Итерируясь по срезу с элемента после "//" заменяем элементы на "*"

	for i := 0; i < len(a); i++ {
		if i <= 6 {
			byteslice = append(byteslice, a[i])
			continue
		} else {
			a[i] = 42
			byteslice = append(byteslice, a[i])
		}

	}

	convstring := string(byteslice)
	return convstring
}
